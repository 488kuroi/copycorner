<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function home(){
      $people = [ 'uno', 'due', 'tre' ];
        return view('welcome')->withPeople( $people );
    }

    public function about()
    {
      return view('about');
    }
}
