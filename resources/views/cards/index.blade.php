@extends( 'layout' )

@section( 'content' )

  <h1> Ciao sono l'indice delle carte </h1>

    @foreach( $cards as $card )
    <a href=" {{ $card -> path() }} "><h4> {{ $card -> title }} </h4></a>
    @endforeach
@stop()
