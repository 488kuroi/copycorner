@extends( 'layout' )

@section( 'content' )

  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <h1 class="text-center"> {{ $card -> title }} </h1>
      <ul class="list-group">
          @foreach ( $card->notes as $note )
            <li class="list-group-item"> {{ $note->body }}
              <a class="pull-right" href=" # "> Created by {{ $note->user->username }}</a>
              <em class="pull-right">&nbsp;-&nbsp;</em>
              <a class="pull-right" href=" {{ $note->pathToEdit() }} "> Edit this note </a>
            </li>
          @endforeach
      </ul>
      <br />
      <hr>
      <br />
      <h3> Add a new note </h3>
      <form method="POST" action="/cards/{{ $card->id }}/notes">
        <div class="form-group">
          <textarea name="body" class="form-control"> {{ old( 'body' ) }} </textarea>
          {{ csrf_field() }}
          <br />
          <button type="submit" class="btn btn-primary">Add the note</button>
        </div>
      </form>
      @if( count( $errors ) > 0 )
        <ul>
          @foreach ($errors->all() as $error )

          <li> {{ $error }} </li>

          @endforeach
        </ul>

      @endif

    </div>
  </div>

@stop
