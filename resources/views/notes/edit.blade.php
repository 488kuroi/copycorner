@extends( 'layout' )

@section( 'content' )

<div class="container">
  <div class="col-md-12">
    <h1> Edit note <h1>
      <form method="POST" action="/notes/{{ $note->id }}">
       {{ method_field( 'PATCH' ) }}
        <div class="form-group">
          <textarea name="body" class="form-control"> {{ $note -> body }} </textarea>
          <br />
          <button type="submit" class="btn btn-primary">Edit the note</button>
        </div>
      </form>
  </div>

</div>


@stop
